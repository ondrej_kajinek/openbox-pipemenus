#!/usr/bin/env lua

--[[
--
-- Author: OndraK
--
-- This piece of lua code can be distributed under the terms of GNU GPL v3
--
--]]

local selfPath = debug.getinfo(1).source:gsub("@", "")
local selfDir = selfPath:gsub("[^/]+$", "")

package.path = os.getenv("HOME") .. "/programming/lua/?.lua;" .. selfDir .. "?.lua;" .. package.path

require "libs/common"
require "libs/decorators"
require "libs/string"
local iconSet = require "assets/iconSet"
local l10n = require "assets/l10n"
local mpd = require "libs/mpd"
local openboxMenu = require "libs/openboxMenu"
local system = require "libs/system"

-- use only MPD part of l10n
l10n = l10n[systemLanguage()].mpd
-- use only MPD icons
iconSet = iconSet.mpd

local albumartSize = 80
local albumartName = "albumart.png"
local cmds = {
	clear = "mpc clear",
	convertAlbumart = "convert %s -resize %dx%d %s",
	findAdd = 'mpc findadd %s "%s"',
	loadPlaylist = 'mpc load %s',
	next = "mpc next",
	play = "mpc play",
	playTrack = "mpc play %d",
	previous = "mpc prev",
	random = "mpc random",
	repeatPlaylist = "mpc repeat",
	seek = "mpc seek %d",
	self = selfPath .. " %s",
	toggle = "mpc toggle"
}


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- helper functions	-- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function convertButton(songDir, image)
	local imagePath = system.path(songDir, image:bashEscape())
	local albumartPath = system.path(songDir, albumartName)
	openboxMenu.button(image, cmds.convertAlbumart:format(imagePath, albumartSize, albumartSize, albumartPath))
end

local function modeControls()
	openboxMenu.button(mpd.optionString(l10n.random, "random"), cmds.random, iconSet.random)
	openboxMenu.button(mpd.optionString(l10n.repeating, "repeat"), cmds.repeatPlaylist, iconSet.repeatPlaylist)
end

local function mpdQueryMenu(tag, value)
	local findAdd = cmds.findAdd:format(tag, value)
	local replace = {
		cmds.clear,
		findAdd,
		cmds.play
	}
	openboxMenu.beginMenu(string.format('mpd-%s-%s', tag, value), value)
	openboxMenu.title(value)
	openboxMenu.button(l10n.replace, replace)
	openboxMenu.button(l10n.add, findAdd)
	openboxMenu.endMenu()
end

local function playbackControls()
	openboxMenu.button(l10n.previousTrack, cmds.previous, iconSet.skipBackward)
	openboxMenu.button(l10n.playPause, cmds.toggle, iconSet.playbackPause)
	openboxMenu.button(l10n.fromBeginning, cmds.seek:format(0), iconSet.seekBackward)
	openboxMenu.button(l10n.nextTrack, cmds.next, iconSet.skipForward)
end


local function playlistControls()
	openboxMenu.subPipemenu("mpd-current-playlist", l10n.currentPlaylist, cmds.self:format("current-playlist"))
	openboxMenu.subPipemenu("mpd-saved-playlists", l10n.savedPlaylists, cmds.self:format("saved-playlists"))
	openboxMenu.subPipemenu("mpd-query", l10n.mpdQuery, cmds.self:format("mpd-query"))
end

local function otherControls()
	openboxMenu.subPipemenu("mpd-albumarts", l10n.availableAlbumarts, cmds.self:format("albumart-convert"))
end

local function switchPlaylistAction(playlistName)
	return {
		cmds.clear,
		cmds.loadPlaylist:format(playlistName:bashEscape()),
		cmds.play
	}
end

local function savedPlaylistControls(playlist, parent)
	if type(playlist) == "string" then
		local fullName = mpd.fullPlaylistName(playlist, parent)
		openboxMenu.button(playlist, switchPlaylistAction(fullName))
	elseif type(playlist) == "table" then
		local fullPlaylistName = mpd.fullPlaylistName(playlist.name, parent)
		openboxMenu.beginMenu(string.format("mpd-playlists-%s", fullPlaylistName), playlist.name)
		openboxMenu.title(playlist.name)
		for _, subplaylist in ipairs(playlist.content) do
			savedPlaylistControls(subplaylist, fullPlaylistName)
		end
		openboxMenu.endMenu()
	end
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- module functions  -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local albumartConvert = decorator(openboxMenu.pipemenu(l10n.availableAlbumarts)) ..
function()
	local imagesAvailable = false
	for dir, image in mpd.availableAlbumarts() do
		convertButton(dir, image)
		imagesAvailable = true
	end
	if not imagesAvailable then
		openboxMenu.item(l10n.noImagesFound)
	end
end

local createControls = decorator(openboxMenu.pipemenu(mpd.currentSong() or l10n.notPlaying)) ..
function()
	playbackControls()
	openboxMenu.separator()
	modeControls()
	openboxMenu.separator()
	playlistControls()
	openboxMenu.separator()
	otherControls()
end

local currentPlaylist = decorator(openboxMenu.pipemenu(l10n.currentPlaylist)) ..
function()
	local playlist = mpd.currentPlaylist()
	for _, album in ipairs(playlist) do
		openboxMenu.beginMenu(string.format("mpd-playlist-%s", album.name:lower()), album.name)
		openboxMenu.title(album.name)
		for _, track in ipairs(album.tracks) do
			openboxMenu.button(track.name, cmds.playTrack:format(track.number))
		end
		openboxMenu.endMenu()
	end
end

local function help()
	io.stderr:write([[mpd_control script usage:
mpd_control [OPTION]

Available options:
	controls		Creates menu with playback controls
	saved-playlists		Shows list of saved playlists, provides playlist switching functionality
	current-playlist	Shows songs in current playlist, sorted by albums, provides track switching
	mpd-query		Shows possible MPD queries for playlist switching / appending
	albumart-convert	Lists images in current song directory, able to convert images into albumarts
	help			Prints this help
]])
end

local mpdQuery = decorator(openboxMenu.pipemenu(l10n.mpdQuery)) ..
function()
	local alphabet_composers = {}
	local alphabet_keys = {}
	for composer in mpd.composers() do
		-- match one character, even with multibyte encoding
		local initial = composer:match("[\33-\127\192-\255][\128-\191]*")
		if not alphabet_composers[initial] then
			alphabet_keys[#alphabet_keys + 1] = initial
			alphabet_composers[initial] = {}
		end
		local box = alphabet_composers[initial]
		box[#box + 1] = composer
	end
	for i, initial in ipairs(alphabet_keys) do
		openboxMenu.beginMenu(string.format('mpd-composer-%s', initial), initial:upper())
		openboxMenu.title(l10n.composers_starting:format(initial:upper()))
		for _, composer in ipairs(alphabet_composers[initial]) do
			mpdQueryMenu('composer', composer)
		end
		openboxMenu.endMenu()
	end
end

local savedPlaylists = decorator(openboxMenu.pipemenu(l10n.savedPlaylists)) ..
function()
	local playlists = mpd.savedPlaylists()
	local empty = true
	for _, playlist in pairs(playlists) do
		savedPlaylistControls(playlist)
		empty = false
	end
	if empty then
		openboxMenu.item(l10n.noPlaylistsFound)
	end
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- -- -- MAIN	-- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function main(option, ...)
	local actions =
	{
		["controls"] = createControls,
		["saved-playlists"] = savedPlaylists,
		["current-playlist"] = currentPlaylist,
		["albumart-convert"] = albumartConvert,
		["mpd-query"] = mpdQuery,
		["help"] = help
	}
	local option = option or "controls"
	local action = actions[option] or help
	action({...})
end

main(unpack({...}))

